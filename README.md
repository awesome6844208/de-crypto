<h1 align="center">Hi 👋, I'm Megh Deb</h1>
<h3 align="center">A passionate entrepreneur and programmer from Kolkata</h3>

<p align="left"> <a href="https://github.com/ryo-ma/github-profile-trophy"><img src="https://github-profile-trophy.vercel.app/?username=Megh2005&theme=matrix&no-bg=true" alt="Megh2005" /></a> </p>

- 🔭 I’m currently working on **[Heritage Institute of Technology](https://heritageit.edu)**

- 🌱 I’m currently learning **[Kotlin](https://kotlinlang.org/)**

- 👯Connect me through my **[Social Links](https://linktr.ee/meghdeb)**

- 🤝 I’m also available at **[Google for Developers](https://g.dev/MeghDeb)**

- 👨‍💻 All of my projects are available at **[GitHub](https://github.com/Megh2005)**

- 📝 I write articles on **[Designing](https://www.linkedin.com/posts/megh-deb-20637a2a1_connections-comments-activity-7169906081660436480-_bpa?utm_source=share&utm_medium=member_desktop)**

- 💬 Ask me about **Marketing , Android**

- 📫 How to reach me **[iammeghdeb@gmail.com](mailto:iammeghdeb@gmail.com?subject=I%20want%20to%20connect)**

- 📄 Know about my experiences **[click here](https://old-portfolio-dj58tt4aw-megh-debs-projects.vercel.app/)**



<h3 align="left">Languages and Tools:</h3>
<p align="left"> <a href="https://getbootstrap.com" target="_blank" rel="noreferrer"> <img src="https://upload.wikimedia.org/wikipedia/commons/b/b2/Bootstrap_logo.svg" alt="bootstrap" width="40" height="30"/> </a> <a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a> <a href="https://www.djangoproject.com/" target="_blank" rel="noreferrer"> <img src="https://cdn.worldvectorlogo.com/logos/django.svg" alt="django" width="40" height="40"/> </a> <a href="https://www.docker.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-original-wordmark.svg" alt="docker" width="40" height="40"/> </a> <a href="https://firebase.google.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/firebase/firebase-icon.svg" alt="firebase" width="40" height="40"/> </a> <a href="https://cloud.google.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/google_cloud/google_cloud-icon.svg" alt="gcp" width="40" height="40"/> </a> <a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> <a href="https://www.mongodb.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mongodb/mongodb-original-wordmark.svg" alt="mongodb" width="40" height="40"/> </a> <a href="https://nextjs.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/nextjs/nextjs-plain.svg" alt="nextjs" width="40" height="40"/> </a> <a href="https://www.python.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a> <a href="https://www.tensorflow.org" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/tensorflow/tensorflow-icon.svg" alt="tensorflow" width="40" height="40"/> </a> <a href="https://zapier.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/zapier/zapier-icon.svg" alt="zapier" width="40" height="40"/> </a> </p>


<p><img align="left" src="https://github-readme-stats.vercel.app/api/top-langs?username=Megh2005&show_icons=true&title_color=efefef&text_color=68d511&bg_color=151515&cache_seconds=1860&locale=en&layout=compact" alt="Megh2005" /></p>

<p>&nbsp;<img align="center" src="https://github-readme-stats.vercel.app/api?username=Megh2005&show_icons=true&theme=dark&locale=en" alt="Megh2005" /></p>

<p><img align="center" src="https://github-readme-streak-stats.herokuapp.com/?user=Megh2005&theme=highcontrast" alt="Megh2005" /></p>
<a href="https://www.buymeacoffee.com/iammeghdeb" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>
